  rule sliver_implant_strings {
	meta:
		description = "Possible sliver C2 implant"
		triage_description = "Found more than 1000 random strings ending in .go. This have been seen in sliver C2 implants."
		triage_score = 8
		author = "Randsec"
	strings:
		$regex_go = /[\x00-\x7F]{8}\.go/
	
	condition:
		#regex_go > 1000
}
