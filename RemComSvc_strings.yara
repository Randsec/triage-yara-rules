rule RemComSvc_strings
{
    meta:
        triage_description = "Detects strings used in remcom hacking tool."
        triage_score = 10
        description = "Remcom tool has some hardcoded strings."
        author = "Randsec"
    strings:
        $hex_pipe = { 5c 5c 2e 5c 70 69 70 65 5c 25 73 25 73 25 64 }
        $hex_pipe_remcom_communication = { 5c 5c 2e 5c 70 69 70 65 5c 52 65 6d 43 6f 6d 5f 63 6f 6d 6d 75 6e }

    condition:
        $hex_pipe and $hex_pipe_remcom_communication
}

